import React, { useContext } from 'react';
import { Link } from "react-router-dom";
import LoginButton from '../ui/LoginButton';
import UserButton from '../ui/UserButton';
import BookIcon from '../icons/BookIcon';
import PlusIcon from '../icons/PlusIcon';
import NotebookIcon from '../icons/NotebookIcon';
import {AvatarContext} from '../context/AvatarContext';

function Header() {
  const [avatar, setAvatar] = useContext(AvatarContext);
  const lsAvatar = localStorage.getItem("avatar");
  if (lsAvatar) {
    setAvatar(lsAvatar);
  }
  const isUserLoggedIn = () => {
    let jwt = localStorage.getItem("jwt");
    return jwt;
  }

  const menuItem = () => {
    if (isUserLoggedIn()) {
      return (<UserButton />);
    } else {
      return (<LoginButton />);
    }
  }

  return (
    <header className="text-gray-700 body-font">
      <div className="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center">
        <Link to="/" className="flex title-font font-medium items-center text-gray-900 mb-4 md:mb-0">
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-10 h-10 text-white p-2 bg-indigo-500 rounded-full" viewBox="0 0 24 24">
            <path d="M12 2L2 7l10 5 10-5-10-5zM2 17l10 5 10-5M2 12l10 5 10-5"></path>
          </svg>
          <span className="ml-3 text-xl">strapi blog</span>
        </Link>
        <nav className="md:mr-auto md:ml-4 md:py-1 md:pl-8 md:border-l md:border-gray-400	flex flex-wrap items-center text-base justify-center">
          <Link to="/" className="ml-2 mr-2 lg:ml-2 lg:mr-4 hover:text-gray-900">
            <BookIcon />
            Read
            </Link>
          <Link to="/my" className="ml-2 mr-2 lg:mr-4 lg:ml-4 hover:text-gray-900">
            <NotebookIcon />
            My Posts</Link>
          <Link to="/new" className="ml-2 mr-2 lg:ml-4 lg:mr-4 hover:text-gray-900">
            <PlusIcon />
            New Post</Link>
        </nav>
        {menuItem()}
      </div>
    </header>
  );
}

export default Header;
