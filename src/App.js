import React, { useContext } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Header from './components/Header';
import Footer from './components/Footer';
import Home from './pages/Home';
import Login from './pages/Login';
import Signup from './pages/Signup';
import Post from './pages/Post';
import myPosts from './pages/MyPosts';
import NewPost from './pages/NewPost';
import {AlertProvider } from './context/AlertContext';
import {AvatarProvider} from './context/AvatarContext';
import MyPosts from './pages/MyPosts';

function App() {
  return (
    <div className="App" >
      <AlertProvider>
        <AvatarProvider>
          <Router>
            <Header />
            <div className="container px-5 py-4 mx-auto flex flex-col text-center w-full mb-12">
              <Switch>
                <Route exact path="/">
                  <Home />
                </Route>
                <Route path="/my">
                  <MyPosts />
                </Route>
                <Route path="/new">
                  <NewPost />
                </Route>
                <Route path="/post/:id">
                  <Post />
                </Route>
                <Route path="/login">
                  <Login />
                </Route>
                <Route path="/signup">
                  <Signup />
                </Route>
              </Switch>
            </div>
            <Footer />
          </Router>
        </AvatarProvider>
      </AlertProvider>
    </div>
  );
}

export default App;
