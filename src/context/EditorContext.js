import React, { useState, createContext } from 'react';

export const EditorContext = createContext();

export const EditorProvider = (props) => {
    const [editor, setEditor] = useState("");
    return(
        <EditorContext.Provider value={[editor, setEditor]}>
            {props.children}
        </EditorContext.Provider>
    );
}
