import React, { useState, createContext } from 'react';

export const AlertContext = createContext();

export const AlertProvider = (props) => {
    const [alerts, setAlerts] = useState([]);
    return(
        <AlertContext.Provider value={[alerts, setAlerts]}>
            {props.children}
        </AlertContext.Provider>
    );
}
