import React, { useContext } from 'react';
import LogoutIcon from '../icons/LogoutIcon';
import UserIcon from '../icons/UserIcon';
import { AvatarContext } from '../context/AvatarContext';
import { Link, useHistory } from 'react-router-dom';

function UserButton(props) {
  const [avatar, setAvatar] = useContext(AvatarContext); 
  //const avatar = localStorage.getItem("avatar");
  const history = useHistory();
  const logout = () => {
    localStorage.removeItem("jwt");
    localStorage.removeItem("avatar");
    setAvatar("");
    history.push("/login");
  }
  /**
   * 
   */
  return (
       <div class="mt-8 xs:mt-10 sm:mt-10 md:mt-0 dropdown inline-block relative cursor-pointer">
          <span className="flex title-font font-medium items-center text-gray-900 mb-4 md:mb-0">
            <img className="rounded-full" width="50px" src={avatar} />
            <svg className="hidden md:block ml-2 fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/> </svg>
          </span>
          <ul class="w-48 dropdown-menu absolute pt-2 left--4 hidden text-gray-700 pt-1">
            <li class=""><Link to="/" class="rounded-t bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="#">
              <UserIcon />
              Profile</Link></li>
            <li class=""><a onClick={logout} class="rounded-b bg-gray-200 hover:bg-gray-400 py-2 px-4 block whitespace-no-wrap" href="#">
              <LogoutIcon />
              Log Out
            </a></li>
          </ul>
        </div>
  );
}

export default UserButton;