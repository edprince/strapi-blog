import React, { useContext } from 'react';
import { AlertContext } from '../context/AlertContext';

function Alert(props) {
  const [alerts, setAlerts] = useContext(AlertContext); 
  let bgColor;
  let border;
  let fgColor;
  let title;
  const close = () => {
    setAlerts([]);
  }
  console.log(props);
  if (props.type === "error") {
    title = "Holy Smokes!";
    bgColor = "bg-red-100";
    border = "border-red-400";
    fgColor = "text-red-700";
  }
  if (props.type === "warning") {
    title = "Ye be warned!";
    bgColor = "bg-orange-100";
    border = "border-orange-400";
    fgColor = "text-orange-700";
  }
  if (props.type === "info") {
    title="Information Centre";
    bgColor = "bg-teal-100";
    border = "border-teal-400";
    fgColor = "text-teal-700";
  }
  if (props.type === "success") {
    title="Yeeehaa";
    bgColor = "bg-green-100";
    border = "border-green-400";
    fgColor = "text-green-700";
  }
  return (
    <div className={bgColor + " " + border + " " + fgColor + " border px-4 py-3 rounded relative text-left mt-5"} role="alert">
      <p className="font-bold">{title}</p>
      <span className="block sm:inline">{props.message}</span>
      <span className="absolute top-0 bottom-0 right-0 px-4 py-3">
        <svg onClick={close} className={fgColor + " fill-current h-5 w-6"} role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"/></svg>
      </span>
    </div>
  );
}

export default Alert;
