import React, { useState, useEffect } from 'react';
import BlogCard from '../components/BlogCard';
import axios from 'axios';
import {
  Link
} from "react-router-dom";
import Post from './Post';

function Home() {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    const fetchPosts = async () => {
      const result = await axios.get(process.env.REACT_APP_SERVER + "/posts");
      setPosts(result.data);
    };
    fetchPosts();
  }, []);

  return (
    <div className="flex flex-wrap -m-4">
      {posts.map((post) => {     
           console.log("Entered");                 
           // Return the element. Also pass key     
           return (<BlogCard id={post.id} category={post.category.toUpperCase()} title={post.title} preview={post.body.substring(0, 150)} />) 
        })}
    </div>
  );
}

export default Home;
