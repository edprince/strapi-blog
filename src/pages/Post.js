import React, { useState, useEffect } from 'react';
import BlogCard from '../components/BlogCard';
import ReactMarkdown from 'react-markdown';
import axios from 'axios';
import {
  Link,
  useParams
} from "react-router-dom";

function Post() {
  const [post, setPost] = useState({user: { username: ""}});
  let { id } = useParams();
  useEffect(() => {
    const fetchPost = async () => {
      const result = await axios.get(process.env.REACT_APP_SERVER + "/posts/" + id);
      setPost(result.data);
    };
    fetchPost();
  }, []);
  /*
        <div className="rounded-lg h-64 overflow-hidden">
        </div>
  <img alt="content" className="object-cover object-center h-full w-full" src={process.env.REACT_APP_SERVER + "/uploads/P1040109_edit2_dd433012fe.jpeg"} />
  */
  return (
      <div className="lg:w-3/6 mx-auto">
        <div className="flex flex-col sm:flex-row">
          <div className="sm:w-full sm:pl-8 py-0 sm:py-8 md:py-8 md:pl-0 text-center sm:text-left">
          <h2 className="tracking-widest title-font font-medium text-sm text-gray-500 text-center uppercase">{post.category}</h2>
          <h1 className="pb-10 pt-2 sm:text-3xl text-2xl font-medium title-font text-gray-900 text-center">{post.title}</h1>
            <div className="content leading-relaxed text-md text-justify">
            {<ReactMarkdown source={post.body} />}
            </div>
            <a className="text-indigo-500 inline-flex items-center">Learn More
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-4 h-4 ml-2" viewBox="0 0 24 24">
                <path d="M5 12h14M12 5l7 7-7 7"></path>
              </svg>
            </a>
          </div>
        </div>
          <div className="sm:w-full text-center sm:pr-8 sm:py-8">
            <div className="w-20 h-20 rounded-full inline-flex items-center justify-center bg-gray-200 text-gray-400">
              <img className="rounded-full" width="70px" src={post.user.avatar} />
            </div>
            <div className="flex flex-col items-center text-center justify-center">
              <h2 className="font-medium title-font mt-4 text-gray-900 text-lg">{post.user.username}</h2>
              <div className="w-12 h-1 bg-indigo-500 rounded mt-2 mb-4"></div>
              <div>
                <p className="w-48 mx-auto text-base text-gray-600">{post.user.bio}</p>
              </div>
            </div>
          </div>
      </div>
  );
}

export default Post;
