import React, { useState, useContext, useEffect } from 'react';
import BlogCard from '../components/BlogCard';
import Alert from '../ui/Alert';
import { AlertContext } from '../context/AlertContext';
import { AvatarContext } from '../context/AvatarContext';

import axios from 'axios';
import {
  Link,
  Redirect,
  useHistory
} from "react-router-dom";

function Login(props) {
  const [email, setEmail] = useState("dan");
  const [password, setPassword] = useState("password");
  const [alerts, setAlerts] = useContext(AlertContext); 
  const [avatar, setAvatar] = useContext(AvatarContext);
  let history = useHistory();



  const login = async () => {
    try {
      const result = await axios.post(process.env.REACT_APP_SERVER + "/auth/local", {
        identifier: email,
        password: password
      });
      setJWT(result.data.jwt);
      handleAvatar(result.data.user.avatar);
      setAlerts([]);
      history.push("/");
    } catch (err) {
      console.log(err.response.status);
      let status = err.response.status;
      if (status === 400) {
        err.message = "Login details incorrect";
      } 
      setAlerts([...alerts, {type: "error", message: err.message}])
    }
  };

  const setJWT = (token) => {
    localStorage.setItem("jwt", token);
  }

  const handleAvatar = (avatar) => {
    localStorage.setItem("avatar", avatar);
    setAvatar(avatar);
  }

  return (
    <section className="text-gray-700 body-font">
      <div className="container px-5 py-24 mx-auto flex flex-wrap items-center">
        <div className="lg:w-3/5 md:w-1/2 md:pr-16 lg:pr-0 pr-0">
          <h1 className="title-font font-medium text-3xl text-gray-900">Log in to create blog posts with strapi blog</h1>
          <p className="leading-relaxed mt-4">Poke slow-carb mixtape knausgaard, typewriter street art gentrify hammock starladder roathse. Craies vegan tousled etsy austin.</p>
        </div>
        <div className="lg:w-2/6 md:w-1/2 rounded-lg p-8 flex flex-col md:ml-auto w-full mt-10 md:mt-0">
          <h2 className="text-gray-900 text-lg font-medium title-font mb-5">Log In</h2>
          <input onChange={e => setEmail(e.target.value)} className="bg-gray-200 rounded border border-gray-400 focus:outline-none focus:border-indigo-500 text-base px-4 py-2 mb-4" placeholder="Email" type="email" />
          <input onChange={e => setPassword(e.target.value)} className="bg-gray-200 rounded border border-gray-400 focus:outline-none focus:border-indigo-500 text-base px-4 py-2 mb-4" placeholder="Password" type="password" />
          <button onClick={login} className="text-white bg-indigo-500 border-0 py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded text-lg">Log In</button>
          {alerts.map((alert) => {     
              console.log("Entered");                 
              // Return the element. Also pass key     
              return (<Alert type={alert.type} message={alert.message} />) 
            })}
          <Link to="/signup" className="text-xs text-gray-500 mt-3">No account? Sign up</Link>
        </div>
      </div>
    </section>
  );
}

export default Login;
