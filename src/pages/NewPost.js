import React, { useState, useEffect, useContext } from 'react';
import Editor from '../components/Editor';
import { AlertContext } from '../context/AlertContext';
import Alert from '../ui/Alert';
import axios from 'axios';
import {
  Link,
  useParams
} from "react-router-dom";

function NewPost() {
  const [title, setTitle] =  useState("");
  const [category, setCategory] = useState("");
  const [content, setContent] = useState("");
  const [alerts, setAlerts] = useContext(AlertContext);
  const handleTitle = (title) => {
    setTitle(title);
  }
  const handleCategory = (category) => {
    setCategory(category);
  }

  const handleContent = (content) => {
    setContent(content);
  }

  const submit = async () => {
    const token = localStorage.getItem("jwt");
    const config = {
      headers: { Authorization: `Bearer ${token}` }
    };

    const payload = {
      title: title,
      body: content,
      category: category
    };

    try {
      const {data} = await axios
        .post(process.env.REACT_APP_SERVER + '/posts', payload, config);

      console.log(data);
      setAlerts([...alerts, {type: "success", message: "Post submitted"}]);
    } catch (err) {
      let status = err.response.status;
      if (status === 401) {
        err.message = "You need to create an account to post!";
      }
      setAlerts([...alerts, {type: "error", message: err.message}]);
    }
  }

  return (
    <div className="container lg:px-48 py-4 mx-auto flex flex-col text-center w-full mb-12">
      <input onChange={e => handleTitle(e.target.value)} className="outline-none text-2xl py-4 font-medium title-font text-gray-900" value={title} placeholder="Post Title..."type="text"/>
      <input onChange={e => handleCategory(e.target.value)} className="outline-none py-4 font-medium title-font text-gray-700" value={category} placeholder="CATEGORY"type="text"/>
      <Editor callback={content => handleContent(content)} />
      <button onClick={submit} className="bg-indigo-500 p-2 w-24 text-white rounded mt-4 hover:bg-indigo-700">Submit</button>
      {alerts.map((alert) => {     
          console.log("Entered");                 
          // Return the element. Also pass key     
          return (<Alert type={alert.type} message={alert.message} />) 
        })}
    </div>
  );
}

export default NewPost;