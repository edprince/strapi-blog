import React, { useState, useContext, useEffect } from 'react';
import BlogCard from '../components/BlogCard';
import Alert from '../ui/Alert';
import { AlertContext } from '../context/AlertContext';
import axios from 'axios';
import {
  Link,
  Redirect,
  useHistory
} from "react-router-dom";

function Signup(props) {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [alerts, setAlerts] = useContext(AlertContext); 
  let history = useHistory();

  const signup = async () => {
    try {
      const result = await axios.post(process.env.REACT_APP_SERVER + "/auth/local/register", {
        username: username,
        email: email,
        password: password
      });
      setAlerts([...alerts, {type: "success", message: "You are signed up!"}]);
    } catch (err) {
      console.log(err);
      setAlerts([...alerts, {type: "error", message: err.message}])
    }
  };



  return (
    <section className="text-gray-700 body-font">
      <div className="container px-5 py-24 mx-auto flex flex-wrap items-center">
        <div className="lg:w-3/5 md:w-1/2 md:pr-16 lg:pr-0 pr-0">
          <h1 className="title-font font-medium text-3xl text-gray-900">Sign up to create blog posts with strapi blog</h1>
          <p className="leading-relaxed mt-4">Poke slow-carb mixtape knausgaard, typewriter street art gentrify hammock starladder roathse. Craies vegan tousled etsy austin.</p>
        </div>
        <div className="lg:w-2/6 md:w-1/2 rounded-lg p-8 flex flex-col md:ml-auto w-full mt-10 md:mt-0">
          <h2 className="text-gray-900 text-lg font-medium title-font mb-5">Sign Up</h2>
          <input onChange={e => setUsername(e.target.value)} className="bg-gray-200 rounded border border-gray-400 focus:outline-none focus:border-indigo-500 text-base px-4 py-2 mb-4" placeholder="Username" type="text" />
          <input onChange={e => setEmail(e.target.value)} className="bg-gray-200 rounded border border-gray-400 focus:outline-none focus:border-indigo-500 text-base px-4 py-2 mb-4" placeholder="Email" type="email" />
          <input onChange={e => setPassword(e.target.value)} className="bg-gray-200 rounded border border-gray-400 focus:outline-none focus:border-indigo-500 text-base px-4 py-2 mb-4" placeholder="Password" type="password" />
          <button onClick={signup} className="text-white bg-indigo-500 border-0 py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded text-lg">Sign Up</button>
          {alerts.map((alert) => {     
              console.log("Entered");                 
              // Return the element. Also pass key     
              return (<Alert type={alert.type} message={alert.message} />) 
            })}
          <Link to="/login" className="text-xs text-gray-500 mt-3">Already have an account? Log in</Link>
        </div>
      </div>
    </section>
  );
}

export default Signup;
